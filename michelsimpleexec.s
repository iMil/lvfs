section .data

sh: db '/bin/sh', 0

section .text

global _start

_start:
	mov eax, 0xb
	mov ebx, sh
	xor edx, edx
	push edx
	push ebx ; /bin/sh, 0
	mov ecx, esp
	int 0x80
