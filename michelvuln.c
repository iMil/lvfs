#include <stdio.h>
#include <string.h>

void
innocent(char *argv)
{
	char buffer[128];
	register int esp asm("esp");
	register int ebp asm("ebp");
	register int *p = (int *)esp, raddr = ebp + 4;

	strcpy(buffer, argv);

	printf("ebp: %x, esp: %x\n", ebp, esp);
	for (;(int)p <= raddr; p++) {
		printf("%x: %x\n", (int)p, *p);
	}
}

int
main(int argc, char *argv[])
{
	if (argc > 1)
		innocent(argv[1]);
}
