CFLAGS=-ggdb
LDFLAGS=-static

ARCH := $(shell uname -m)

# build sur une machine GNU/Linux récente
ifeq ($(ARCH), x86_64)
	CFLAGS += -m32 -O0 -fno-stack-protector -fno-pic -masm=intel
	LDFLAGS += -m32 -no-pie -z execstack
endif

.PRECIOUS: %.bin

%.bin: %.s
	nasm -felf32 -o $@.o $<
	ld -melf_i386 -o $@ $@.o

%.sc: %.bin
	objdump -D $< | sed -nE 's/[0-9a-f]+:\s+([0-9a-f\ ]+)\s.+/\ \1/pig' | \
		xargs | sed 's/^/\\x/; s/\ /\\x/g' > $@
