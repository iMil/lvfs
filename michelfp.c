#include <stdio.h>

int
main(int argc, char *argv[])
{
	int (*f)() = (int(*)())argv[1];

	f();

	return 0;
}
