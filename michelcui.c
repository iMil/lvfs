/*
 * Code d'exemple du L(v)FS S01E20
 * Compiler de cette façon : cc -m32 -masm=intel -o michelcui michelcui.c
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int
cui()
{
	__asm__("mov eax, gs:0x14");
}

int
main(int argc, char *argv[])
{
	int pid;

	printf("uh? %x\n", cui());
	if ((pid = fork()) < 0) {
		fprintf(stderr, "fork error");
		exit(EXIT_FAILURE);
	}
	if (pid == 0) {
		printf("UH???? %x\n", cui());
	}
	wait(NULL);

	exit(EXIT_SUCCESS);
}
